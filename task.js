let students = [
    {
      name: "Dendy",
      prov: "Jawa Timur",
      age: 21,
      isSingle: true,
    },
    {
      name: "Aceng",
      prov: "Cirebon",
      age: 27,
      isSingle: false,
    },
    {
      name: "Mbah Dito",
      prov: "Goa",
      age: 1000,
      isSingle: false,
    },
    {
      name: "Afi",
      prov: "Bonbin",
      age: 22,
      isSingle: false,
    },
    {
      name: "Imam",
      prov: "Jawa Barat",
      age: 17,
      isSingle: true,
    },
  ];
  
  function isJawaBarat(loc) {
    return loc == "Jawa Barat" ? loc : "bukan Jawa Barat";
  }
  
  function isAgeGreater22(age) {
    return age < 22 ? "di bawah 22" : "lebih dari sama dengan 22";
  }
  
  function isSingle(status) {
    return status == true ? "single" : "menikah";
  }
  
  function printOutput(cb1, cb2, cb3) {
    for (const key in students) {
      let myLocation = cb1(students[key].prov);
      let myAge = cb2(students[key].age);
      let myStatus = cb3(students[key].isSingle);
      if (
        myLocation == "Jawa Barat" &&
        myAge == "di bawah 22" &&
        myStatus == "single"
      ) {
        console.log(
          `Nama saya ${students[key].name}, saya tinggal di ${myLocation}, umur saya ${myAge}, dan saya ${myStatus} loh. CODE buat nilam`
        );
      }
    }
  }
  
  printOutput(isJawaBarat, isAgeGreater22, isSingle);
  